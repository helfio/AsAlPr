#!/usr/bin/python
# -*- coding: utf8 -*-
# cliente para nova.astrometry.net
import sys, os
import json, requests




def CheckFileExistence(nombrearchivo):
  if os.path.exists(nombrearchivo):
    return 1
  elif not os.path.exists(nombrearchivo):
    file(nombrearchivo, 'w').close()
    return 0



def IniciaConfiguracion():
  if os.path.exists("./cliente.cfg"):
    import ConfigParser
    global config
    config = ConfigParser.RawConfigParser()
    config.read('./cliente.cfg')
    return 1
  elif not os.path.exists("./cliente.cfg"):
    import shutil
    shutil.copy("./cliente.cfg.new","./cliente.cfg")
    ErrorSQL()
    sys.exit()


def RefreshSessionKey():
  IniciaConfiguracion()
  varApiKey = config.get('general', 'api_key')
  saludo = {'request-json': json.dumps({'apikey':varApiKey})}
  urlapilogin = config.get('general', 'url_login')
  import requests
  global s
  s = requests.Session()
  r = s.post(urlapilogin, saludo)
  print json.loads(r.text)['session']
  salida = r.text
  global sessionkey
  sessionkey = json.loads(salida)['session']
  


def SubirArchivo(parasubir):
  RefreshSessionKey()
  urlapisubidaarchivo = config.get('general','url_upload')
  files = [('file',(parasubir, open(parasubir,'rb'),'octet-stream'))]
  datos = {'request-json':json.dumps({'session':sessionkey,'publicly_visible':'n'})}
  respuesta = s.post(urlapisubidaarchivo,data = datos, files = files)
  print respuesta.status_code
  print respuesta.text
  print " "
  return json.loads(respuesta.text)['subid']


def EstadoProceso(codigo):
  consulta = {'request-json':json.dumps({'session':sessionkey})}
  urlapisubid = config.get('general','url_subid')
  respuesta = s.post(urlapisubid + str(codigo),data = consulta)
  print respuesta.status_code
  print respuesta.text
  return respuesta.text
  
  
def PedirDatosImg(codigo):
  consulta = {'request-json':json.dumps({'session':sessionkey})}
  urlapidatos = config.get('general','url_datos')
  respuesta = s.post(urlapidatos + str(codigo) + '/calibration/',data = consulta)
  print respuesta.status_code
  print respuesta.text
  return respuesta.text


def PedirWCS(codigo):
  urlapi = config.get('general','url_wcs')
  respuesta = requests.get(urlapi+str(codigo), stream=True)
  with open(nombrearchivo+'_wcs.fits', 'wb') as salida:
    for trozo in respuesta.iter_content(chunk_size=1024):
      if trozo:
        salida.write(trozo)
        salida.flush()
  

def PedirRDLS(codigo):
  urlapi = config.get('general','url_rdls')
  respuesta = requests.get(urlapi+str(codigo), stream=True)
  with open(nombrearchivo+'_rdls.fits', 'wb') as salida:
    for trozo in respuesta.iter_content(chunk_size=1024):
      if trozo:
        salida.write(trozo)
        salida.flush()


def PedirAXY(codigo):
  urlapi = config.get('general','url_axy')
  respuesta = requests.get(urlapi+str(codigo), stream=True)
  with open(nombrearchivo+'_axy.fits', 'wb') as salida:
    for trozo in respuesta.iter_content(chunk_size=1024):
      if trozo:
        salida.write(trozo)
        salida.flush()


def PedirNombres(codigo):
  consulta = {'request-json':json.dumps({'session':sessionkey})}
  urlapidatos = config.get('general','url_nombres')
  respuesta = s.post(urlapidatos + str(codigo) + '/annotations/',data = consulta)
  print respuesta.status_code
  texto = str(json.dumps(respuesta.text, sort_keys=True)).replace('\\','').replace(',',',\n').split('{')
  with open(nombrearchivo+'_nombres.txt', 'wb') as salida:
    for t in texto:
      salida.write(t)
      salida.write('\n')
      salida.write('{')
      salida.write('\n')


def PedirEsquema(codigo):
  urlapi = config.get('general','url_esquema')
  respuesta = requests.get(urlapi+str(codigo), stream=True)
  with open(nombrearchivo+'_esquema.jpg', 'wb') as salida:
    for trozo in respuesta.iter_content(chunk_size=1024):
      if trozo:
        salida.write(trozo)
        salida.flush()


def PedirCorr(codigo):
  urlapi = config.get('general','url_corr')
  respuesta = requests.get(urlapi+str(codigo), stream=True)
  with open(nombrearchivo+'_corr.fits', 'wb') as salida:
    for trozo in respuesta.iter_content(chunk_size=1024):
      if trozo:
        salida.write(trozo)
        salida.flush()


def PedirObjField(codigo):
  consulta = {'request-json':json.dumps({'session':sessionkey})}
  urlapidatos = config.get('general','url_datos')
  respuesta = s.post(urlapidatos + str(codigo) + '/objects_in_field/',data = consulta)
  print respuesta.status_code
  print respuesta.text
  return respuesta.text


def PedirJPL(objeto,fechai,horai,fechaf,horaf): 
  # Formato de fecha: 2000-12-31
  # Formato de hora 00:00
  # Está puesto con el paso mínimo: 1 minuto
  url = 'http://ssd.jpl.nasa.gov/horizons_batch.cgi?batch=1&COMMAND=%27' + objeto + '%27&MAKE_EPHEM=%27YES%27%20%20%20%20&TABLE_TYPE=%27OBSERVER%27&START_TIME=%27' + fechai + '%20' + horai + '%27&STOP_TIME=%27' + fechaf + '%20' + horaf + '%27&STEP_SIZE=%2760%20m%27%20%20%20%20&QUANTITIES=%271%27&CSV_FORMAT=%27YES%27&ANG_FORMAT=%27DEG%27'
  respuesta = requests.get(url)
  datos = repuesta.text.split('$$SOE')[1].split('$$EOE')[0].split(',\n')
  # devuelve un vector con un dato por celda
  return datos






if len(sys.argv) == 2:
  archivo = sys.argv[1]
elif len(sys.argv) > 2:
  #ErrorMuchosArg()
  print "Demasiados argumentos."
  print " "
  sys.exit()
elif len(sys.argv) == 1:
  #ErrorNoArg()
  print "Tiene que pasar un archivo como argumento."
  print " "
  sys.exit()

nombrearchivo = archivo.split('.')[0]
subid = SubirArchivo(archivo)

import time 
while True:
  info = json.loads(EstadoProceso(subid))
  print info['jobs']
  if (len(info['jobs']) >= 1):
    if (isinstance(info['jobs'][0],int)):
      print 'Ya tenemos jobID: ' + str(info['jobs'][0])
      if (len(info['job_calibrations']) >= 1) and (isinstance(info['job_calibrations'][0][0],int)):
        print 'Ya tenemos código de calibración: ' + str(info['job_calibrations'][0][1])
        time.sleep(2)
        PedirDatosImg(info['jobs'][0])
        PedirWCS(info['jobs'][0])
        PedirRDLS(info['jobs'][0])
        PedirAXY(info['jobs'][0])
        PedirNombres(info['jobs'][0])
        PedirEsquema(info['jobs'][0])
        PedirCorr(info['jobs'][0])
        PedirObjField(info['jobs'][0])
        break
  time.sleep(1)

